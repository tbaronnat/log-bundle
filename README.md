###### How to use
- Add LoggableInterface to each entity you want to log
- Create your own instance of LogManager, extending LogManager or AbstractLogManager of this bundle if needed to add more data to LogObject, or to configure another way to exclude fields if you want
- Create your own Log.php Entity who extends of AbstractLog and implements LogInterface
- Create a Repository associated to your entity, and put LogRepositoryTrait inside, or use LogRepository on this file
- Your current User class should implements LogCreatorInterface
- Display logs:


###### Excluded fields annotation
## Use Doctrine annotation @LogExcludedField to exclude a specific field to log in your entity 
```
    /**
     * @LogExcludedField()
     * @ORM\Column(name="excluded_field", type="string", length=255, nullable=true)
     */
    protected ?string $excludedField = null;
```

###### Exemples:
## Display all logs depending on specifics classes, for one user.
````
    {% include '@TBaronnatLog/logs/_display_classes_user_log.html.twig' with {
        'classes': ['App\\Entity\\User', 'App\\Entity\\Documents'],
        'user': resource.user
    } %}
````

````
    {% if resource is not null and resource.id is not null %}
        {% include '@TBaronnatLog/logs/_display_resource_log.html.twig' with {
            'resource': resource
        } %}
    {% endif %}
````

````
    {% include '@TBaronnatLog/logs/_display_resources_log.html.twig' with {
        'resources':  resources,
        'logClasses': ['App\\Entity\\Documents']
    } %}
````
