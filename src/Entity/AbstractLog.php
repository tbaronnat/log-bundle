<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\LogBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractLog implements LogInterface
{
    /**
     * @ORM\Column(name="id_log", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    protected DateTime $date;

    /**
     * @ORM\Column(name="action", type="string", length=50, nullable=false)
     */
    protected string $action;

    /**
     * @ORM\Column(name="object_id", type="bigint", nullable=true)
     */
    protected ?int $objectId = null;

    /**
     * @ORM\Column(name="object_class", type="string", length=255, nullable=true)
     */
    protected ?string $objectClass = null;

    /**
     * @ORM\Column(name="data", type="text", nullable=false)
     */
    protected string $data;

    /**
     * @ORM\Column(name="label", type="string", length=512, nullable=false)
     */
    protected string $label;
    protected ?LogCreatorInterface $creator;


    public function __construct()
    {
        $this->date = new DateTime();
    }

    public static function create(): static
    {
        return new static();
    }

    public function __toString()
    {
        return $this->getAction() . ' ' . $this->getObjectClass() . ' ' . $this->getObjectId();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(?int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getObjectClass(): string
    {
        return $this->objectClass;
    }

    public function setObjectClass(string $objectClass): self
    {
        $this->objectClass = $objectClass;

        return $this;
    }

    public function getObjectToDisplay(): string
    {
        return sprintf('%s - %s', $this->objectId, $this->objectClass);
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCreator(): ?LogCreatorInterface
    {
        return $this->creator;
    }

    public function setCreator(?LogCreatorInterface $creator): self
    {
        $this->creator = $creator;
        return $this;
    }
}
