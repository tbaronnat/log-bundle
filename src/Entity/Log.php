<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\LogBundle\Entity;

class Log extends AbstractLog implements LogInterface
{
}
