<?php

/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Entity;

use DateTime;

interface LogInterface
{
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';

    public function getId(): int;

    public function getDate(): DateTime;

    public function setDate(DateTime $date): self;

    public function getAction(): string;

    public function setAction(string $action): self;
    public function getObjectId(): ?int;

    public function setObjectId(?int $objectId): self;

    public function getObjectClass(): string;

    public function setObjectClass(string $objectClass): self;

    public function getObjectToDisplay(): string;

    public function getData(): string;

    public function setData(string $data): self;

    public function getLabel(): ?string;

    public function setLabel(?string $label): self;

    public function getCreator(): ?LogCreatorInterface;

    public function setCreator(?LogCreatorInterface $creator): self;
}
