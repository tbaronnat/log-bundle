<?php

/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Entity;

interface LoggableInterface
{
    public function getId(): ?int;
    public function getLabel(): string;
}
