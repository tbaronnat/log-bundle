<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Extension;

use TBaronnat\LogBundle\Manager\LogManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class LogExtension extends AbstractExtension
{
    public function __construct(private readonly LogManagerInterface $logManager)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_logs_for_object', [
                $this->logManager->getLogsRepository(),
                'getLogsForObject',
            ]),
            new TwigFunction('get_logs_for_resources', [
                $this->logManager->getLogsRepository(),
                'getLogsForResources',
            ]),
            new TwigFunction('get_logs_for_user_and_class', [
                $this->logManager->getLogsRepository(),
                'getLogsForClassNameAndUser',
            ]),
        ];
    }
}
