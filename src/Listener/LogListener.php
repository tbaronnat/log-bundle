<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Listener;

use TBaronnat\LogBundle\Entity\LogCreatorInterface;
use TBaronnat\LogBundle\Entity\LoggableInterface;
use TBaronnat\LogBundle\Entity\LogInterface;
use TBaronnat\LogBundle\Manager\LogManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LogListener
{
    public function __construct(
        protected readonly LogManagerInterface $logManager,
        protected readonly TokenStorageInterface $tokenStorage
    ) {
    }

    public function supports(LifecycleEventArgs $eventArgs, mixed $user): bool
    {
        return $eventArgs->getObject() instanceof LoggableInterface && $user instanceof LogCreatorInterface;
    }

    public function preRemove(LifecycleEventArgs $eventArgs): void
    {
        $this->onDoctrinePostEvent($eventArgs, LogInterface::ACTION_DELETE);
    }

    public function postUpdate(LifecycleEventArgs $eventArgs): void
    {
        $this->onDoctrinePostEvent($eventArgs, LogInterface::ACTION_UPDATE);
    }

    public function postPersist(LifecycleEventArgs $eventArgs): void
    {
        $this->onDoctrinePostEvent($eventArgs, LogInterface::ACTION_CREATE);
    }

    protected function onDoctrinePostEvent(LifecycleEventArgs $eventArgs, string $action): void
    {
        $user = $this->tokenStorage->getToken()?->getUser();
        if ($this->supports($eventArgs, $user)) {
            $this->logManager->logChanges(
                $action,
                $eventArgs->getObject(),
                $user //Should implements LogCreatorInterface
            );
        }
    }
}
