<?php

/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Manager;

use Symfony\Contracts\Translation\TranslatorInterface;
use TBaronnat\LogBundle\Entity\LoggableInterface;
use TBaronnat\LogBundle\Entity\LogInterface;
use TBaronnat\LogBundle\Entity\LogCreatorInterface;
use TBaronnat\LogBundle\Model\LogResult;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Persistence\ObjectRepository;

abstract class AbstractLogManager implements LogManagerInterface
{
    protected LogInterface $log;
    protected bool $autoFlush = true;

    public function __construct(
        protected readonly EntityManagerInterface $em,
        protected readonly TranslatorInterface $translator
    ) {
    }

    public function init(LoggableInterface $object, LogCreatorInterface $creator, string $action): void
    {
        $this->log = new ($this->getLogsEntityClass());
    }

    public function logChanges(
        string $action,
        LoggableInterface $object,
        LogCreatorInterface $creator
    ): void {
        $this->init($object, $creator, $action);
        $logs = [];

        switch ($action) {
            case LogInterface::ACTION_CREATE:
                $logs = $this->getCreateLogs($object);
                break;
            case LogInterface::ACTION_UPDATE:
                $logs = $this->getUpdateLogs($object);
                break;
            case LogInterface::ACTION_DELETE:
                $logs = $this->getDeleteLogs($object);
                break;
        }

        if (!empty($logs)) {
            $this->log = $this->log
                ->setData(json_encode($logs, JSON_PRETTY_PRINT))
                ->setAction($action)
                ->setLabel($this->getObjectLabel($object))
                ->setObjectClass(get_class($object))
                ->setObjectId($object->getId())
                ->setCreator($creator)
            ;

            if ($this->autoFlush) {
                $this->em->persist($this->log);
                $this->em->flush();
            }
        }
    }

    public function getCreateLogs(LoggableInterface $object): array
    {
        return LogResult::create('NULL', $this->getObjectValues($object), 'entity')->toArray();
    }

    public function getUpdateLogs(LoggableInterface $object): array
    {
        $logs = [];
        foreach ($this->em->getUnitOfWork()->getEntityChangeSet($object) as $field => $changes) {
            if (in_array($field, $this->getExcludedFields($object))) {
                continue;
            }
            $log = LogResult::create(
                $this->getRealValue($object, $field, $changes[0]),
                $this->getRealValue($object, $field, $changes[1]),
                $field
            )->toArray();
            $logs[$field] = $log[$field];
        }
        return $logs;
    }

    public function getDeleteLogs(LoggableInterface $object): array
    {
        return LogResult::create($this->getObjectValues($object), 'NULL', 'entity')->toArray();
    }

    public function getLogsRepository(): ObjectRepository
    {
        return $this->em->getRepository($this->getLogsEntityClass());
    }

    protected function getObjectValues(LoggableInterface $object): array
    {
        $data = [];
        $reflectionClass = new \ReflectionClass($object);
        $properties = $reflectionClass->getProperties();

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            if (in_array($propertyName, $this->getExcludedFields($object))) {
                continue;
            }
            $property->setAccessible(true);
            $propertyValue = $property->getValue($object);

            if ($propertyValue instanceof PersistentCollection) {
                continue;
            }

            $data[$propertyName] = $this->getRealValue(
                $object,
                $propertyName,
                $propertyValue
            );
        }

        return $data;
    }

    protected function getRealValue(?LoggableInterface $object, ?string $propertyName, mixed $value): mixed
    {
        if ($value instanceof LoggableInterface) {
            return $this->getObjectLabel($value, true);
        }

        if ($value instanceof \DateTime) {
            $value = $value->format('d/m/Y H:i:s');
            return str_replace(' 00:00:00', '', $value);
        }

        if (is_bool($value)) {
            return $this->translator->trans($value ? 'logs.yes' : 'logs.no', [], 'logs');
        }

        if (is_string($value) || is_integer($value)) {
            return (string)$value;
        }

        if (is_object($value) && method_exists($value, '__toString')) {
            return $value->__toString();
        }

        return $value;
    }


    protected function getObjectLabel(LoggableInterface $object, bool $withId = false): string
    {
        $label = $withId ? sprintf(' (%s)', $object->getId()) : '';
        return sprintf('%s%s', $object->getLabel(), $label);
    }

    protected function getExcludedFields(LoggableInterface $object): array
    {
        //Implement your own logic
        return [];
    }

    abstract protected function getLogsEntityClass(): string;
}
