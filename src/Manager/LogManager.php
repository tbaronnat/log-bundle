<?php

/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Manager;

use Doctrine\Common\Annotations\AnnotationReader;
use TBaronnat\LogBundle\Annotation\LogExcludedField;
use TBaronnat\LogBundle\Entity\Log;
use TBaronnat\LogBundle\Entity\LoggableInterface;

class LogManager extends AbstractLogManager
{
    protected function getLogsEntityClass(): string
    {
        return Log::class;
    }

    protected function getExcludedFields(LoggableInterface $object): array
    {
        $annotationReader = new AnnotationReader();
        $ref = new \ReflectionClass($object);
        $fields = [];

        foreach ($ref->getProperties() as $property) {
            $annotation = $annotationReader->getPropertyAnnotation($property, LogExcludedField::class);
            if ($annotation instanceof LogExcludedField) {
                $fields[] = $property->getName();
            }
        }
        return $fields;
    }
}
