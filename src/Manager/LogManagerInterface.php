<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Manager;

use TBaronnat\LogBundle\Entity\LoggableInterface;
use Doctrine\Persistence\ObjectRepository;
use TBaronnat\LogBundle\Entity\LogCreatorInterface;

interface LogManagerInterface
{
    public function init(LoggableInterface $object, LogCreatorInterface $creator, string $action): void;

    public function logChanges(
        string $action,
        LoggableInterface $object,
        LogCreatorInterface $creator
    ): void;

    public function getCreateLogs(LoggableInterface $object): array;

    public function getUpdateLogs(LoggableInterface $object): array;

    public function getDeleteLogs(LoggableInterface $object): array;

    public function getLogsRepository(): ObjectRepository;
}
