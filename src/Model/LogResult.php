<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Model;

class LogResult
{
    public mixed $from = null;
    public mixed $to = null;
    public string $field;

    public function getFrom(): mixed
    {
        return $this->from;
    }

    public function setFrom(mixed $from): self
    {
        $this->from = $from;
        return $this;
    }

    public function getTo(): mixed
    {
        return $this->to;
    }

    public function setTo(mixed $to): self
    {
        $this->to = $to;
        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;
        return $this;
    }

    public static function create(mixed $from, mixed $to, string $field): self
    {
        $logResult = new self();
        $logResult
            ->setFrom($from)
            ->setTo($to)
            ->setField($field);
        return $logResult;
    }

    public function toArray(): array
    {
        return
            [
                $this->field => [
                    'from' => $this->from,
                    'to'   => $this->to
                ]
            ];
    }
}
