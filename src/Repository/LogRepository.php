<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Repository;

use TBaronnat\LogBundle\Trait\LogRepositoryTrait;
use Doctrine\ORM\EntityRepository;

class LogRepository extends EntityRepository
{
    use LogRepositoryTrait;
}
