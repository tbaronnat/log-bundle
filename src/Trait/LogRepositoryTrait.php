<?php
/*
 * This file is part of the TBaronnatLogBundle app.
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */
namespace TBaronnat\LogBundle\Trait;

trait LogRepositoryTrait
{
    public function getLogsForObject(object $object, ?int $limit = null): array
    {
        $qb = $this->createQueryBuilder('l')
            ->where('l.objectId =:objectId')
            ->andWhere('l.objectClass=:objectClass')
            ->addOrderBy('l.id', 'DESC')
            ->setParameter('objectId', $object->getId())
            ->setParameter('objectClass', get_class($object));

        if (method_exists($object, 'getUser') && $object->getUser() !== null) {
            $qb->andWhere('l.user=:user')
                ->setParameter('user', $object->getUser());
        }

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function getLogsForClassNameAndUser(
        int $userId,
        array $classesNames = [],
        ?int $limit = null
    ): array {
        $qb = $this->createQueryBuilder('l')
            ->where('l.user =:user')
            ->andWhere('l.objectClass in (:objectClasses)')
            ->addOrderBy('l.id', 'DESC')
            ->setParameter('user', $userId)
            ->setParameter('objectClasses', $classesNames);

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function getLogsForResources($resources, array $classes = [], ?int $limit = null): array
    {
        $tabIds = [];
        foreach ($resources as $resource) {
            $tabIds[] = $resource->getId();
        }
        $qb = $this->createQueryBuilder('l')
            ->andWhere('l.objectClass IN (:objectClass)')
            ->addOrderBy('l.id', 'DESC')
            ->setParameter('objectClass', $classes)
        ;

        if (!empty($tabIds)) {
            $qb = $this->createQueryBuilder('l')
                ->andWhere('l.objectId IN (:objectId)')
                ->setParameter('objectId', $tabIds)
            ;
        }

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
